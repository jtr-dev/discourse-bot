const builder = require('botbuilder');

import { greetings, help, none, personal, post } from './dialogs'
import { DiscordConnector } from 'discord-botbuilder';
new DiscordConnector({ token: process.env.DISCORD_APP_TOKEN });

var core: any = {};

core.model = process.env.LUIS_MODEL_API;
core.recognizer = new builder.LuisRecognizer(core.model);
core.dialog = new builder.IntentDialog({ recognizers: [core.recognizer] });
core.connector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});
core.bot = new builder.UniversalBot(core.connector);

// core.bot.set('localizerSettings', { defaultLocale: "en" })
// Anytime the major version is incremented any existing conversations will be restarted.
core.bot.use(builder.Middleware.dialogVersion({ version: 1.0, resetCommand: /^reset/i }));

core.bot.dialog('/', core.dialog)
    .matches('Greeting', greetings)
    .matches('Help', help)
    .matches('Personal', personal)
    .matches('Post', post)
    .matches('None', none)
    .onDefault(session => {
            session.send(`Sorry, ${session.message.text} is not recognized or supported currently`)
    });


export {
    core
}