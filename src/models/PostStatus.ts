export interface PostStatus {
    "success": string;
    "topic_status_update": string | null;
}
