export type RequestPromise<T> = Promise<{
    error?: string | undefined;
    body: T | undefined;
}>;

export interface HandleResponse<T> {
    error?: string | undefined;
    body: T | undefined;
}   
