export enum Categories {
    Zero,
    One,
    Lounge,
    SiteFeedback,
    Staff,
    LookingForWork,
    LookingToHire
}
