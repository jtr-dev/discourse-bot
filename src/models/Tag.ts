export declare namespace Tag {
    interface Extras {
        tag_groups: TagGroup[];
    }

    interface TagGroup {
        id: number;
        name: string;
        tags: Tag[];
    }

    interface Tag {
        id: string;
        text: string;
        count: number;
        pm_count: number;
    }
}

export interface ITags {
    tags: Tag.Tag[];
    extras: Tag.Extras;
}
