import * as express from "express";
import { core } from "./botcore";
import * as request from 'request';
import * as cheerio from 'cheerio';


module.exports = (app: express.Router) => {
    app.post("/api/messages", core.connector.listen());

    app.get("/status", (req, res) => {
        res.send(`[ip:port] ${process.env.IP || 'localhost'}:${process.env.PORT || 1337}`)
    })

};
