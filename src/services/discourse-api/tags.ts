import { Discourse } from "./discourse";
import { ITags, Tag } from "../../models/Tag";

export class Tags {
    discourse: Discourse = new Discourse;
    constructor() { }

    getAll = async (): Promise<string[] | undefined> => {
        const data = await this.discourse.get<ITags>('/tags.json')
        if (data && data.body && !data.error) {
            const tagList: Tag.Tag[] = data.body.tags
            const tags = tagList.map(tag => tag.text)
            return tags
        }
        return undefined;
    }
}

