import { Discourse } from "./discourse-api/discourse";
import Fuse from "fuse.js";

interface FuseResult {
    item: number;
    matches: Match[];
}

interface Match {
    indices: number[][];
    value: string;
}

type TagFuse = string[]

const tagList = async () => {
    const discourse = new Discourse
    return await discourse.tags.getAll()
}

const fuze = async () => {
    const options: Fuse.FuseOptions<TagFuse> = {
        shouldSort: true,
        threshold: 0.1,
        includeMatches: true
    };

    const tags = await tagList();
    if (tags) {
        return new Fuse(tags, options)
    }
}

export const parseTags = async (hostname, $) => {
    const tags: string[] = parseTagsByDomain(hostname, $)
    return tags
}

const parseTagsByDomain = ($, domain) => {
    const defaultTags = [ 'LTH', 'Remote' ]
    const domains = domainsList();
    const domainToParse = domains.find(d => d.hostname === domain)
    if (domainToParse) {
        const { func } = domainToParse;
        const tags: string[] = func($)
        tags.unshift(...defaultTags)
        return tags
    }
    return defaultTags
}

const tryToFindValue = (search) => {
    try {
        const { matches } = search.find(x => x.hasOwnProperty('matches'))
        const { value } = matches.find(x => x.hasOwnProperty('value'))
        return value
    } catch (e) {
    }
}

const domainsList = () =>
    [
        { hostname: "letsworkremotely.com", func: letsWorkRemotely },
        { hostname: "remoteok.io", func: remoteOk },
    ]



/**
 * domainList Funcs
 * 
 */

const letsWorkRemotely = ($): string[] => {
    const tags = $('#job-detail-content > div.job-detail-info > ul > li.category > div > div.content').text()
    return [ tags ]
}

const remoteOk = ($): string[] => {
    let tags: string[] = []
    const job = $('#jobsboard')
    job.each((_, el) => {
        // #job-71806 > td.tags > a:nth-child(2) > div > h3
        $(el).find('.tags > a').each((_, el) => {
            const tag = $(el).text()
            tags.push(tag)
        })
    })
    return tags
}



