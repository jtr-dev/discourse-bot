import { greetings } from './greetings'
import { help } from './help'
import { none } from './none'
import { personal } from './personal'
import { post } from './post'

export {
    greetings,
    help,
    none,
    personal,
    post,
}
