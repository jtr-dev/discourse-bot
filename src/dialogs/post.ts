import * as builder from 'botbuilder'
import * as cheerio from 'cheerio'
import * as request from 'request'
import { Discourse } from '../services/discourse-api/discourse'
import { SingleBulkFlag } from '../models/SingleBulkFlag';
import { SingleBulk } from '../models/SingleBulk';
import { Categories } from '../models/Categories';
import { parseTags } from '../services/tag-parser'

const discourse = new Discourse();

export const post = [
    (session, args, next) => {
        const { type }: { type: string } =
            (args.entities.length > 0)
                ? args.entities.find(e => e.type)
                : { type: '' }

        if (!type) {
            session.userData.choice = null;
            builder.Prompts.choice(session, "Single or Bulk?", "Single|Bulk")
        } else {
            session.userData.choice = (SingleBulk[ type ] || SingleBulkFlag[ type ])
            next({ type })
        }
    },
    (session, result, next) => {
        if (result.type in SingleBulkFlag) {
            next({ type: result.type })
        } else {
            session.userData.choice = session.userData.choice || SingleBulk[ result.response.index || result.response.entity.toLowerCase() ]

            if (session.userData.choice === SingleBulk.single) {
                builder.Prompts.text(session, "Give me the url")
            } else {
                builder.Prompts.text(session, "Give me the urls, separate by comma")
            }
        }
    },
    (async (session, result) => {
        let response: string;
        if (result.type) {
            const str = session.message.text;
            const index = str.indexOf('-');
            response = str.slice(index + 3);
        } else {
            response = result.response;
        }

        const { choice } = session.userData
        if (choice === SingleBulk.single) {
            const result = await createUrl(response)
            session.send(result)
        } else {
            const urls: string[] = response.split(',')
            if (urls.length <= 1) {
                session.send('Please separate by comma')
            } else {
                await Promise.all(urls.map(async (url: string) => {
                    const result = await createUrl(url)
                    session.send(result)
                }))
            }
        }
    })
]

const createUrl = async (url: string) => {
    const { title, tags }: { title: string, tags: string[] } = await scrapeUrl(url)
    const raw = `[${title}](${url})`
    const post = await discourse.topic.createPost({ title, raw, category: Categories.LookingToHire, tags })
    if (post && post.body && !post.error) {
        const id = post.body[ 'topic_id' ]
        const status = await discourse.topic.updatePostStatus(
            id, { status: "closed", enabled: true }
        )
        if (status && !status.error) {
            return `Post successful and closed!`
        }
    }

    return `Post failed: ${post.error}`
}

const scrapeUrl = (url: string): Promise<{ title: string, tags: string[] }> => {
    return new Promise((response) => {
        request.get(url, async (err, res, body: string) => {
            const domain = new URL(url).hostname
            const $ = cheerio.load(body);
            const title: string = await parseTitle($)
            const tags: string[] = await parseTags($, domain)
            response({ title, tags })
        })
    })
}



const parseTitle = async ($) =>
    $("title").text();

